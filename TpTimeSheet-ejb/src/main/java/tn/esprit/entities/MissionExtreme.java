package tn.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;
import tn.esprit.entities.Mission;

/**
 * Entity implementation class for Entity: MissionExtreme
 *
 */
@Entity

public class MissionExtreme extends Mission implements Serializable {

	
	private String emailFacturation;
	private double tauxJournalierMoyen;
	private static final long serialVersionUID = 1L;

	public MissionExtreme() {
		super();
	}   
	public String getEmailFacturation() {
		return this.emailFacturation;
	}

	public void setEmailFacturation(String emailFacturation) {
		this.emailFacturation = emailFacturation;
	}   
	public double getTauxJournalierMoyen() {
		return this.tauxJournalierMoyen;
	}

	public void setTauxJournalierMoyen(double tauxJournalierMoyen) {
		this.tauxJournalierMoyen = tauxJournalierMoyen;
	}
   
}
