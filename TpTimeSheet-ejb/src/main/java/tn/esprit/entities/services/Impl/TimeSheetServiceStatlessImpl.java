package tn.esprit.entities.services.Impl;

import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.entities.Departement;
import tn.esprit.entities.Employe;
import tn.esprit.entities.Mission;
import tn.esprit.entities.Timesheet;
import tn.esprit.entities.services.TimesheetServiceLocal;
import tn.esprit.entities.services.TimesheetServiceRemote;

@Stateless
public class TimeSheetServiceStatlessImpl implements TimesheetServiceLocal, TimesheetServiceRemote {

	@PersistenceContext(unitName= "TpTimeSheet-ejb")
	EntityManager em;
	
	@Override
	public int ajouterMission(Mission mission) {
		// TODO Auto-generated method stub
		em.persist(mission);
		return mission.getId();
	}

	@Override
	public void affecterMissionADepartement(int missionId, int depId) {
		// TODO Auto-generated method stub
		
		Mission mess=em.find(Mission.class, missionId);
		Departement dep=em.find(Departement.class, depId);
		mess.setDepartement(dep);
		em.merge(mess);
		

	}

	@Override
	public void ajouterTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin) {
		// TODO Auto-generated method stub
		
		Mission mess=em.find(Mission.class, missionId);
		Employe en=em.find(Employe.class, employeId);
		Timesheet timesheet=new Timesheet();
		timesheet.setValide(true);
		timesheet.setEmploye(en);
		timesheet.setMission(mess);

	}

}
