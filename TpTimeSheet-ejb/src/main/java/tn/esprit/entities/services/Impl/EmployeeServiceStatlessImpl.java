package tn.esprit.entities.services.Impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.entities.Contrat;
import tn.esprit.entities.Departement;
import tn.esprit.entities.Employe;
import tn.esprit.entities.Entreprise;
import tn.esprit.entities.services.EmployeServiceLocal;
import tn.esprit.entities.services.EmployeServiceRemote;

@Stateless
public class EmployeeServiceStatlessImpl implements EmployeServiceLocal, EmployeServiceRemote {

	@PersistenceContext(unitName= "TpTimeSheet-ejb")
	EntityManager em;
	
	@Override
	public int ajouterEmploye(Employe employe) {
		// TODO Auto-generated method stub
		em.persist(employe);
		return employe.getId();
	}

	@Override
	public void affecterEmployeADepartement(int employeId, int depId) {
		// TODO1 Auto-generated method stub
		
		Departement dep=em.find(Departement.class, depId);
		Employe en=em.find(Employe.class, employeId);
		
//		en.getDepartements().add(dep);
		
		dep.getEmployes().add(en);
			em.persist(dep);

	}

	@Override
	public int ajouterContrat(Contrat contrat) {
		// TODO Auto-generated method stub
		em.persist(contrat);
		return contrat.getReference();
	}

	@Override
	public void affecterContratAEmploye(int contratId, int employeId) {
		// TODO Auto-generated method stub
		Contrat contrat=em.find(Contrat.class, contratId);
		Employe en=em.find(Employe.class, employeId);
		contrat.setEmploye(en);
		em.merge(contrat);
		

	}

	@Override
	public String getEmployePrenomById(int employeId) {
		// TODO Auto-generated method stub

		 Query query = em.createQuery("SELECT e.prenom FROM Employe e WHERE e.id = :empId");
	      query.setParameter("empId", employeId);
	      
	      List<String> Deps=query.getResultList();
		
		return Deps.get(0);
	}

	@Override
	public long getNombreEmployeJPQL() {
		// TODO Auto-generated method stub
		 Query query = em.createQuery("SELECT count(e.id) FROM Employe e ");
		 Long count= (Long) query.getSingleResult();

		return count;
	}

	@Override
	public List<String> getAllEmployeNamesJPQL() {
		// TODO Auto-generated method stub
		 Query query = em.createQuery("SELECT distinct e.nom FROM Employe e ");
		return query.getResultList();
		}

}
