package tn.esprit.entities.services;

import java.util.Date;

import javax.ejb.Local;


import tn.esprit.entities.Mission;
@Local
public interface TimesheetServiceLocal {
	public int ajouterMission(Mission mission);
	public void affecterMissionADepartement(int missionId, int depId);
	public void ajouterTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin);

}
