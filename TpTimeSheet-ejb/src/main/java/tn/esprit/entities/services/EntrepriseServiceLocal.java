package tn.esprit.entities.services;

import java.util.List;

import javax.ejb.Local;


import tn.esprit.entities.Departement;
import tn.esprit.entities.Entreprise;

@Local
public interface EntrepriseServiceLocal {

	public int ajouterEntreprise(Entreprise entreprise);
	public int ajouterDepartement(Departement dep);
	public void affecterDepartementAEntreprise(int depId, int entrepriseId);
	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId);
}
