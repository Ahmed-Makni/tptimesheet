package tn.esprit.entities.services.Impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.entities.Departement;
import tn.esprit.entities.Entreprise;
import tn.esprit.entities.services.EntrepriseServiceLocal;
import tn.esprit.entities.services.EntrepriseServiceRemote;

@Stateless
public class EntrepriseServiceStatlessImpl implements EntrepriseServiceLocal, EntrepriseServiceRemote {

	@PersistenceContext(unitName= "TpTimeSheet-ejb")
	EntityManager em;
	
	@Override
	public int ajouterEntreprise(Entreprise entreprise) {
		// TODO Auto-generated method stub
			em.persist(entreprise);
		return entreprise.getId();
	}

	@Override
	public int ajouterDepartement(Departement dep) {
		// TODO Auto-generated method stub
		em.persist(dep);
	
		return dep.getId();
	}

	@Override
	public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
		// TODO Auto-generated method stub
		Departement dep=em.find(Departement.class, depId);
		Entreprise en=em.find(Entreprise.class, entrepriseId);
		dep.setEntreprise(en);
		em.merge(dep);	
		
	}

	@Override
	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {

		Entreprise en=em.find(Entreprise.class, entrepriseId);
	      Query query = em.createQuery("SELECT d.name FROM Departement d WHERE d.entreprise = :EnttId");
	      query.setParameter("EnttId", en);
	      
		return  query.getResultList();
	}

}
